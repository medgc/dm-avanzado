import numpy as np
import pandas as pd
from sklearn.datasets import load_breast_cancer
from sklearn.svm import NuSVC
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from votedperceptron import VotedPerceptron


# cargo la base de datos “Breast Cancer”
data = load_breast_cancer(return_X_y=False)
df = pd.DataFrame(np.c_[data['data'], data['target']],
                  columns= np.append(data['feature_names'], ['target']))

# separo en train y test
train, test = train_test_split(df, shuffle=True)

X_train = train.iloc[:, :-1]
y_train = train.iloc[:, -1]

X_test = test.iloc[:, :-1]
y_test = test.iloc[:, -1]


# predigo con Voted Perceptron y mido el accuracy
perceptron = VotedPerceptron(100)
perceptron.fit(X_train, y_train)
y_pred_vp = perceptron.predict(X_test)
print('Accuracy score VP: ', accuracy_score(y_test, y_pred_vp))


# predigo con SVM no lineal usando RBF y mido el accuracy
svm = NuSVC(gamma='auto')
svm.fit(X_train, y_train)
y_pred_svm = svm.predict(X_test)
print('Accuracy score SVM: ', accuracy_score(y_test, y_pred_svm))


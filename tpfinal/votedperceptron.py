import numpy as np

class VotedPerceptron():
    def __init__(self, T):
        self.T = T
        self.V = []
        self.C = []
        self.k = 0

    def fit(self, X, y):
        y = y.replace(0, -1)

        k = 0
        v = [np.ones_like(X)[0]]
        c = [0]

        for epoch in range(self.T):
            for i in range(len(X)):
                pred = 1 if np.dot(v[k], X.iloc[i]) > 0 else -1
                if pred == y.iloc[i]:
                    c[k] = c[k] + 1
                else:
                    v.append(np.add(v[k], y.iloc[i] * X.iloc[i]))
                    c.append(1)
                    k = k + 1

        self.V = v
        self.C = c
        self.k = k


    def predict(self, X):
        preds = []
        for index, row in X.iterrows():
            x = row.array
            s = 0.0
            for w, c in zip(self.V, self.C):
                s = s + c * np.sign(np.dot(w, x))

            preds.append(np.sign(1 if s >= 0.0 else -1))

        return preds

